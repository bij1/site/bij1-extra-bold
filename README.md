BIJ1 Extra Bold font
====================

This is the BIJ1 Extra Bold font. This font is a fork of Public Sans. The
intention of our changes is to make this font look friendlier and rounder.

This font is available under the Open Font License (OFL).
